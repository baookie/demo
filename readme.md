
A simple REST endpoint for the demonstration

Example usage:

curl -v localhost:8080/demo -H 'Content-type:application/json' -d '{"data": [5, 4, 6, 1]}'

Response:
'{"output":8.774964387392123}'