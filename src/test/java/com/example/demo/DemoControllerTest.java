package com.example.demo;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Test the DemoController endpoints
 */
@SpringBootTest
@AutoConfigureMockMvc
public class DemoControllerTest {

    @Autowired
    private MockMvc mvc;

    /**
     * Test the POST returns the root of the sum of the squares
     * @param testCase
     * @throws Exception
     */
    public void postData(TestCase testCase) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        mvc.perform(MockMvcRequestBuilders.post("/demo")
                    .content(objectMapper.writeValueAsString(testCase.data))
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
                    .andDo(new PostDataResultHanlder(testCase.expectedResult));
    }


    public class PostDataResultHanlder implements ResultHandler {
        double expectedValue;

        public PostDataResultHanlder(double expectedValue) {
            this.expectedValue = expectedValue;
        }

        public void handle(MvcResult var1) throws Exception {
            ObjectMapper objectMapper = new ObjectMapper();

            DemoOutput output = objectMapper.readValue(var1.getResponse().getContentAsString(), DemoOutput.class);
            // Allow for truncation error with a tolerance
            assertTrue(Math.abs(output.output - expectedValue) < 1e-2);
        }
    }

    public class TestCase {
        DemoData data;
        double expectedResult;

        public TestCase(double[] data, double expectedResult) {
            this.data = new DemoData(data);
            this.expectedResult = expectedResult;
        }
    }

    //TODO use Parameterized test instead. Sort out dependencies with SpringBootTest

    @Test
    public void test1() throws Exception {
        postData(new TestCase(new double[] {5, 4, 6, 1}, 8.77));
    }

    @Test
    public void testFail() throws Exception {
        // Test that the test Assertion fails for the wrong expected output
        boolean passed = false;
        try {
            postData(new TestCase(new double[]{5, 4, 6, 1}, 8.70));
        }
        catch (AssertionError error) {
            passed = true;
        }

        assertTrue(passed, "postData should throw an Assertion Error");

    }

    @Test
    public void testEmptyData() throws Exception {
        postData(new TestCase(new double[] {}, 0));
    }

    @Test
    public void testSmallListSize() throws Exception {
        postData(new TestCase(new double[] {-1}, 1));
    }
}
