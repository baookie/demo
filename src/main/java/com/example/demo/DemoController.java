/**
 * @author: stubbst
 * last modified: stubbst
 */

package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * A simple REST endpoint for the demonstration
 *
 */
@RestController
public class DemoController {

    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/demo")
    String all() {
        return "hello world";
    }
    // end::get-aggregate-root[]

    /**
     * A POST endpoint which returns the square root of the sum of squares of the 3 highest inputs
     *
     * Example usage:
     * curl -v localhost:8080/demo -H 'Content-type:application/json' -d '{"data": [5, 4, 6, 1]}'

     * @param numbers A DemoData object containing a list of doubles.
     * @return The square root of the sum of squares of the 3 highest inputs
     */
    @PostMapping("/demo")
    DemoOutput data(@RequestBody DemoData numbers) {
        int count = numbers.data.length;
        double value = Arrays.stream(numbers.data).sorted().skip(Math.max(0, count-3)).map(x -> Math.pow(x, 2)).sum();
        return new DemoOutput(Math.pow(value, 0.5));
    }
}
